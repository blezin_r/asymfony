<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\FOSUserBundle;
use AppBundle\Form\EditUserType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need

        $manager = $this->get('fos_user.user_manager');
        $users = $manager->findUsers();
        $friendsId = [];

        $securityContext = $this->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            foreach( $this->getUser()->getFriends() as $friend )
            {
                $friendsId[] = $friend->getId();
            }
        }
        
        return $this->render('@AppBundle/views/index/index.html.twig', array(
            "users" => $users,
            "friendsId" => $friendsId));
    }

    /**
     * @Route("/profile/edit", name="edituserpage")
     */
    public function editUserAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $currentUser = $this->getUser();
        $newForm = $this->createForm(EditUserType::class, $currentUser);
        $newForm->handleRequest($request);

        if($newForm->isSubmitted() && $newForm->isValid())
        {
            $manager->merge($currentUser);
            $manager->flush();
        }

        return $this->render('@AppBundle/views/Form/Profile/edit_content.html.twig', array("form" => $newForm->createView()));
    }

    /**
     * @Route("/addfriend/{id}", name="addfriend")
     */
    public function addFriendAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $currentUser = $this->getUser();
        $user = $manager->getRepository("AppBundle:User")->find($id);
        $currentUser->addFriend($user);
        $manager->flush();

        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * @Route("/removefriend/{id}", name="removefriend")
     */
    public function removeFriendAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        $currentUser = $this->getUser();
        $user = $manager->getRepository("AppBundle:User")->find($id);
        $currentUser->removeFriend($user);
        $manager->flush();

        return $this->redirect($this->generateUrl('homepage'));
    }

}