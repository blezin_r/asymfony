<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Model\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class EditUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, array('label' => 'Username',
                                                        'required' => false));
        $builder->add('email', EmailType::class, array('label' => 'Email',
                                                       'required' => false));
        $builder->add('age', NumberType::class, array('label' => 'Age',
                                                      'required' => false));
        $builder->add('family', TextType::class, array('label' => 'Family',
                                                       'required' => false));
        $builder->add('race', TextType::class, array('label' => 'Race',
                                                     'required' => false));
        $builder->add('food', TextType::class, array('label' => 'Food',
                                                     'required' => false));
        $builder->add('plainPassword', PasswordType::class, array('label' => 'Password',
                                                                  'required' => false));
    }

}