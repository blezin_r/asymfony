-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  ven. 10 nov. 2017 à 14:24
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `symfony`
--
CREATE DATABASE IF NOT EXISTS `symfony` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `symfony`;

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `age` int(11) NOT NULL,
  `race` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `family` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `food` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `age`, `race`, `family`, `food`) VALUES
(7, 'Maya', 'maya', 'maya@live.fr', 'maya@live.fr', 1, NULL, '$2y$13$93ubYXxGcUAZuHfXlgJFJOkXmA29hWJcm.kbc3ZQv5UV9jmk2iYy.', '2017-11-10 13:58:36', NULL, NULL, 'a:0:{}', 17, 'Abeille', 'Apoidea', 'Nectar'),
(8, 'Fourmiz', 'Fourmiz', 'fourmizlive.fr', 'fourmiz@live.fr', 1, NULL, '$2y$13$3c/dHZbFoRLgV8w02W5MQe1qw2VcIS9H1rr9dmxDwi91ANX2jM3pu', '2017-11-09 17:11:43', NULL, NULL, 'a:0:{}', 19, 'fourmi noire', 'Formicoidea', 'champignon'),
(9, 'Mouskito', 'Mouskito', 'moustiko@live.fr', 'mouskito@live.fr', 1, NULL, '$2y$13$fST116n9IIaCOzWHkBIVs.liMjXG1hK53MrWbWcrfWVz44mXxNAPW', '2017-11-09 22:02:24', NULL, NULL, 'a:0:{}', 29, 'Anophèles', 'Culicidae', 'Sang');

-- --------------------------------------------------------

--
-- Structure de la table `friends`
--

CREATE TABLE `friends` (
  `user_a_id` int(11) NOT NULL,
  `user_b_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `friends`
--

INSERT INTO `friends` (`user_a_id`, `user_b_id`) VALUES
(7, 7),
(7, 9),
(8, 7),
(9, 7),
(9, 8),
(9, 9);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Index pour la table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`user_a_id`,`user_b_id`),
  ADD KEY `IDX_21EE7069415F1F91` (`user_a_id`),
  ADD KEY `IDX_21EE706953EAB07F` (`user_b_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `friends`
--
ALTER TABLE `friends`
  ADD CONSTRAINT `FK_21EE7069415F1F91` FOREIGN KEY (`user_a_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_21EE706953EAB07F` FOREIGN KEY (`user_b_id`) REFERENCES `fos_user` (`id`);
